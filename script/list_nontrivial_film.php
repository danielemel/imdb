<?php
include('header.php');
include('connect.php');

if ( mysqli_select_db($conn, 'imdb')){
    $sql = "SELECT film.filmID, film.cim, film.megjelenesiEv, rendezo.rendezoID, rendezo.rendezoNev, rendezo.szuletesiOrszag FROM film, rendezo WHERE rendezo.szuletesiOrszag != 'USA' AND film.megjelenesiEv > (SELECT AVG(megjelenesiEv) from film) AND film.rendezoID = rendezo.rendezoID";
    $res = mysqli_query($conn, $sql) or die("Hibás utasítás!");

    //html
    echo '<table class="table table-light table-striped">';
    echo '<thead class="thead-dark">';
    echo '<tr>';
    echo '<th scope="col">Film Azonosító</th>';
    echo '<th scope="col">Film Cím</th>';
    echo '<th scope="col">Megjelenési Év</th>';
    echo '<th scope="col">Rendező Azonosító</th>';
    echo '<th scope="col">Rendező Név</th>';
    echo '<th scope="col">Rendező Születésihely</th>';
    echo '<th scope="col">Módosítás</th>';
    echo '<th scope="col">Törlés</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    while(($current_row = mysqli_fetch_assoc($res))) {
        echo '<tr>';
        echo '<td>' . $current_row["filmID"] .'</td>';
        echo '<td>' . $current_row["cim"] . '</td>';
        echo '<td>' . $current_row["megjelenesiEv"] . '</td>';
        echo '<td>' . $current_row["rendezoID"] . '</td>';
        echo '<td>' . $current_row["rendezoNev"] . '</td>';
        echo '<td>' . $current_row["szuletesiOrszag"] . '</td>';
        echo '<form method="post" id="modify_film" action="modify_film_form.php" accept-charset="UTF-8">';
        echo '<td><button type="submit" form="modify_film" class="btn btn-warning" name="filmID" value="'.$current_row["filmID"].'">Módosítás</td>';
        echo '</form>';
        echo'<form method="post" id="delete_film" action="delete_film.php" accept-charset="UTF-8">';
        echo '<td><button type="submit" form="delete_film" class="btn btn-danger" name="filmID" value="'.$current_row["filmID"].'">Törlés</td>';
        echo '</form>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';

    mysqli_free_result($res);
} else {
    die('Nem sikerlt csatlakozni az adatbázishoz');
}

mysqli_close($conn);

include('footer.php');