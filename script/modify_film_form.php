<?php
include("connect.php");
include("fuggvenyek.php");
?>
    <?php
    $v_filmID = $_POST["filmID"];
    $v_clear_filmID = htmlspecialchars($v_filmID);
    $filmadat = get_film($v_clear_filmID);

?>
    <h3>Film szerekesztése:</h3>
    <form method="post" id="filmForm" class="form-inline" action="modify_film_script.php" accept-charset="UTF-8">
        <label for="filmID">Film ID:</label>
        <?php echo '<input type="number" id="filmID" name="filmID" min="1" max="9999" value="'.$filmadat["filmID"].'" readonly="readonly"/>';?>
        <label for="cim">Cím:</label>
        <?php echo '<input type="text" id="cim" name="cim" value="'.$filmadat["cim"].'"/>';?>
        <label for="megjelenesiEv">Megjelenési Év:</label>
        <?php echo '<input type="number" id="megjelenesiEv" name="megjelenesiEv" min="1920" max="2020" value="'.$filmadat["megjelenesiEv"].'"/>';?>
        <label for="korhatar">Korhatár:</label>
       <?php echo'<input type="number" id="korhatar" name="korhatar" min="6" max="18" value="'.$filmadat["korhatar"].'"/>';?>
        <label for="rendezoID1">Rendező ID:</label>
        <?php echo '<input type="number" id="rendezoID1" name="rendezoID" min="1" max="9999" value="'.$filmadat["rendezoID"].'" readonly="readonly"/>';?>
        <label for="studioID">Stúdió ID:</label>
        <?php echo '<input type="number" id="studioID" name="studioID" min="1" max="9999" value="'.$filmadat["studioID"].'" readonly="readonly"/>';?>
        <button type="submit" form="filmForm" class="btn btn-primary">Szerkesztés</button>
    </form>
<?php mysqli_close($conn); ?>