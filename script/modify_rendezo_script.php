<?php

include ('fuggvenyek.php');
imdb_connect();

$v_rendezoID = $_POST['rendezoID'];
$v_rendezoNev = $_POST['rendezoNev'];
$v_szuletesiEv = $_POST['szuletesiEv'];
$v_szuletesiOrszag = $_POST['szuletesiOrszag'];

if ( isset($v_rendezoID) && isset($v_rendezoNev) &&
    isset($v_szuletesiEv) && isset($v_szuletesiOrszag) ) {

    $v_clear_rendezoID = htmlspecialchars($v_rendezoID);
    $v_clear_rendezoNev = htmlspecialchars($v_rendezoNev);
    $v_clear_szuletesiEv = htmlspecialchars($v_szuletesiEv);
    $v_clear_szuletesiOrszag = htmlspecialchars($v_szuletesiOrszag);

    // beszúrjuk az új rekordot az adatbázisba
    $sikeres = modify_rendezo($v_clear_rendezoID, $v_clear_rendezoNev, $v_clear_szuletesiEv, $v_clear_szuletesiOrszag);

    if ($sikeres == false){
        die("Nem sikerült felvinni rekordot.");
    } else {
        header("Location: list_rendezo.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");
}
//END