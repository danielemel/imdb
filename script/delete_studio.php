<?php
include('fuggvenyek.php');
imdb_connect();

$v_studioID = $_POST['studioID'];

if ( isset($v_studioID)) {
    // beszúrjuk az új rekordot az adatbázisba
    $v_clear_studioID = htmlspecialchars($v_studioID);

    $success = remove_studio($v_clear_studioID);
    if($success == false){
        die("Nem sikerült törölni a rekordot.");
    } else {
        header("Location: list_studio.php");
    }
} else {
    error_log("Nincs beállítva valamely érték");

}