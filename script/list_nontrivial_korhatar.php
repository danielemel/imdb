<?php
include('header.php');
include('connect.php');

if ( mysqli_select_db($conn, 'imdb')){
    $sql = "SELECT film.korhatar AS 'korhatar', count(film.korhatar) AS 'korhatarDarab' FROM film, studio WHERE film.studioID=studio.studioID AND studio.alapitasiEv > 2000 GROUP BY film.korhatar";
    $res = mysqli_query($conn, $sql) or die("Hibás utasítás!");

    //html
    echo '<table class="table table-light table-striped">';
    echo '<thead class="thead-dark">';
    echo '<tr>';
    echo '<th scope="col">Korhatár</th>';
    echo '<th scope="col">Filmek Száma</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    while(($current_row = mysqli_fetch_assoc($res))) {
        echo '<tr>';
        echo '<td>' . $current_row["korhatar"] .'</td>';
        echo '<td>' . $current_row["korhatarDarab"] . '</td>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';

    mysqli_free_result($res);
} else {
    die('Nem sikerlt csatlakozni az adatbázishoz');
}

mysqli_close($conn);

include('footer.php');