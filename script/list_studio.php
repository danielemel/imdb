<?php
    include('header.php');
    include('connect.php');

    if ( mysqli_select_db($conn, 'imdb')){

        $sql = "SELECT * FROM studio";
        $res = mysqli_query($conn, $sql) or die("Hibás utasítás!");

        //html
        echo '<table class="table table-light table-striped">';
        echo '<thead class="thead-dark">';
        echo '<tr>';
        echo '<th scope="col">Azonosítószám</th>';
        echo '<th scope="col">Név</th>';
        echo '<th scope="col">Alapítási Év</th>';
        echo '<th scope="col">Módosítás</th>';
        echo '<th scope="col">Törlés</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        while(($current_row = mysqli_fetch_assoc($res))) {
            echo '<tr>';
            echo '<td>' . $current_row["studioID"] .'</td>';
            echo '<td>' . $current_row["studioNev"] . '</td>';
            echo '<td>' . $current_row["alapitasiEv"] . '</td>';
            echo'<form method="post" id="modify_studio" action="modify_studio_form.php" accept-charset="UTF-8">';
            echo '<td><button type="submit" form="modify_studio" class="btn btn-warning" name="studioID" value="'.$current_row["studioID"].'">Módosítás</td>';
            echo '</form>';
            echo'<form method="post" id="delete_studio" action="../script/delete_studio.php" accept-charset="UTF-8">';
            echo '<td><button type="submit" form="delete_studio" class="btn btn-danger" name="studioID" value="'.$current_row["studioID"].'">Törlés</td>';
            echo '</form>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';

        mysqli_free_result($res);
    } else {
        die('Nem sikerlt csatlakozni az adatbázishoz');
    }

    mysqli_close($conn);



    include('footer.php');
//END